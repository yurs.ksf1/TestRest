<h2 align="center">Test de DjangoRestFull</h2>

- Guia de Django
http://www.django-rest-framework.org/tutorial/quickstart/

- Tutorial:
http://www.django-rest-framework.org/tutorial/1-serialization/#working-with-serializers
http://www.django-rest-framework.org/tutorial/2-requests-and-responses/
http://www.django-rest-framework.org/tutorial/3-class-based-views/
http://www.django-rest-framework.org/tutorial/4-authentication-and-permissions/
http://www.django-rest-framework.org/tutorial/5-relationships-and-hyperlinked-apis/
http://www.django-rest-framework.org/tutorial/6-viewsets-and-routers/
http://www.django-rest-framework.org/tutorial/7-schemas-and-client-libraries/
