from django.conf.urls import url, include
from rest_framework import routers
from django.contrib import admin
from tutorial.quickstart import views as views1
from myapp import views

router = routers.DefaultRouter()
router.register(r'users', views1.UserViewSet)
router.register(r'groups', views1.GroupViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^', include('myapp.urls')),
]
