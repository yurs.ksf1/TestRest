git clone https://gitlab.com/yurs.ksf1/TestRest.git
cd tutorial

# Virtual environment
virtualenv -p python3 venv
source venv/bin/activate

pip install -r requirements.txt

python manage.py runserver
